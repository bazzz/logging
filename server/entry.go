package server

import "time"

// Entry is a log entry.
type Entry struct {
	Timestamp   time.Time
	Application string
	Severity    int
	Message     string
}
