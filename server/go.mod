module gitlab.com/bazzz/logging/server

go 1.16

require (
	github.com/google/uuid v1.2.0
	github.com/lib/pq v1.10.0
	github.com/patrickmn/go-cache v2.1.0+incompatible
)
