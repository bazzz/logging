package server

import (
	"sort"
	"time"

	"github.com/google/uuid"
	"github.com/patrickmn/go-cache"
)

// DataStore is an interface representing a persistant data store.
type DataStore interface {
	Add(application string, severity int, message string) error
	ViewRecent() ([]Entry, error)
	ViewByMinSeverity(severity int) ([]Entry, error)
	Applications() ([]string, error)
}

// memoryStore is the default implementation of the DataStore interface.
type memoryStore struct {
	mem *cache.Cache
}

func newMemoryStore() *memoryStore {
	store := memoryStore{
		mem: cache.New((7*24)*time.Hour, 1*time.Hour),
	}
	return &store
}

// Add adds the message to the memoryStore.
func (s *memoryStore) Add(application string, severity int, message string) error {
	entry := Entry{
		Timestamp:   time.Now(),
		Application: application,
		Severity:    severity,
		Message:     message,
	}
	s.mem.SetDefault(uuid.New().String(), &entry)
	return nil
}

// ViewRecent returns the most recent entries. Given that memoryStore cleans up old entries automatically, everything inside is considered recent.
func (s *memoryStore) ViewRecent() ([]Entry, error) {
	entries := []Entry{}
	for _, m := range s.mem.Items() {
		entry := m.Object.(*Entry)
		entries = append(entries, *entry)
	}
	synchronize(entries)
	return entries, nil
}

// ViewByMinSeverity returns the log entries with severity at least as high as parameter severity.
func (s *memoryStore) ViewByMinSeverity(severity int) ([]Entry, error) {
	entries := []Entry{}
	for _, m := range s.mem.Items() {
		entry := m.Object.(*Entry)
		if entry.Severity >= severity {
			entries = append(entries, *entry)
		}
	}
	synchronize(entries)
	return entries, nil
}

// Applications returns a distinct list of all application names.
func (s *memoryStore) Applications() ([]string, error) {
	appmap := make(map[string]bool)
	for _, m := range s.mem.Items() {
		entry := m.Object.(*Entry)
		appmap[entry.Application] = true
	}
	applications := []string{}
	for app := range appmap {
		applications = append(applications, app)
	}
	sort.Strings(applications)
	return applications, nil
}

func synchronize(entries []Entry) {
	sort.Slice(entries, func(i, j int) bool { return entries[i].Timestamp.Sub(entries[j].Timestamp) > 0 })
}
