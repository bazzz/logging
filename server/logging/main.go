package main

import (
	"gitlab.com/bazzz/logging/server"
)

func main() {
	logServer := server.NewDefault()
	panic(logServer.Start())
}
