package server

import (
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"testing"
	"time"
)

func init() {
	server := NewDefault()
	go func() {
		server.store.Add("app1", 4, "first message")
		server.store.Add("app2", 2, "second message")
		server.store.Add("app1", 1, "third message")
		server.store.Add("app2", 3, "fourth message")
		err := server.Start()
		if err != nil {
			panic(err)
		}
	}()
	for i := 1; i <= 10; i++ {
		sev := 3
		if i%3 == 0 {
			sev = 2
		} else if i%2 == 0 {
			sev = 1
		}
		if i >= 9 {
			sev = 4
		}
		server.store.Add("testing", sev, "message number "+strconv.Itoa(i))
		time.Sleep(500 * time.Millisecond)
	}
}

func TestViewErrorAndFatal(t *testing.T) {
	response, err := http.Get("http://localhost:9999/view/error/")
	if err != nil {
		t.Fatal(err)
	}
	defer response.Body.Close()
	data, err := io.ReadAll(response.Body)
	if err != nil {
		t.Fatal(err)
	}
	entries := []Entry{}
	err = json.Unmarshal(data, &entries)
	if err != nil {
		t.Fatal(err)
	}
	expected := 14
	if len(entries) != expected {
		t.Fatal("expected", expected, "entries, but got", len(entries))
	}
}

func TestApplications(t *testing.T) {
	response, err := http.Get("http://localhost:9999/applications/")
	if err != nil {
		t.Fatal(err)
	}
	defer response.Body.Close()
	data, err := io.ReadAll(response.Body)
	if err != nil {
		t.Fatal(err)
	}
	apps := []string{}
	err = json.Unmarshal(data, &apps)
	if err != nil {
		t.Fatal(err)
	}
	if apps[0] != "app1" {
		t.Fail()
	}
	if apps[1] != "app2" {
		t.Fail()
	}
	if apps[2] != "testing" {
		t.Fail()
	}
}
